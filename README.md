# NASM Learning Project

This project is for documenting my learning process of NASM (Netwide Assembler) for `x86_64` Linux systems.

# Useful Links

* 64-bit Linux `syscall` table/list
  * [https://filippo.io/linux-syscall-table/](https://filippo.io/linux-syscall-table/)
* Good learning source
  * [https://github.com/0xAX/asm](https://github.com/0xAX/asm)
* Another good learning source
  * [https://www.conradk.com/codebase/2017/06/06/x86-64-assembly-from-scratch/](https://www.conradk.com/codebase/2017/06/06/x86-64-assembly-from-scratch/)
* System V AMD64 ABI
  * [https://raw.githubusercontent.com/wiki/hjl-tools/x86-psABI/x86-64-psABI-1.0.pdf](https://raw.githubusercontent.com/wiki/hjl-tools/x86-psABI/x86-64-psABI-1.0.pdf)

# Notes

The following are general notes on the NASM syntax and x86\_64/Linux. Whenever the _ABI_ is mentioned, we are refering to the _System V AMD64 ABI_ specification.

## Registers

An `x86_64` processor has the following available general purpose registers:

* `rax`
  * Extended `a` register.
* `rbx`
  * Extended `b` register.
* `rcx`
  * Extended `c` register.
* `rdx`
  * Extended `d` register.
* `rbp`
  * Register base pointer (start of stack).
* `rsp`
  * Register stack pointer (current location in stack, growing downwards).
* `rsi`
  * Register source index (source for data copies).
* `rdi`
  * Register destination index (destination for data copies).
* `r8`
  * Register 8.
* `r9`
  * Register 9.
* `r10`
  * Register 10.
* `r11`
  * Register 11.
* `r12`
  * Register 12.
* `r13`
  * Register 13.
* `r14`
  * Register 14.
* `r15`
  * Register 15.

All the above registers are 64-bit. They may optionally be accessed in different bit sizes:

* 32-bit (lower half of 64-bit)
  * Prefixed with `e` instead of `r`, i.e. `mov eax, r15d` instead of `mov rax, r15`.
* 16-bit (lower half of 32-bit)
  * No prefix instead of `r`, i.e. `mov ax, r15w` instead of `mov rax r15`.
* 8-bit ( lower half of 16-bit)
  * High byte (lower half of 8-bit)
    * Accessed using `ah` and `bh` for last half of data, i.e. `mov ah, r15b` and `mov bh, r15b`.
  * Low byte (upper half of 8-bit)
    * Accessed using `al` and `bl` for first half of data, i.e. `mov al, r15b` and `mov bl, r15b`.

All registers are essentially the same, there is nothing special about them at a low level, just at a high level (such as `rbp` and `rsp` being
used specifically for the _stack_).

It is also imperative to understand that _registers_ and __NOT__ the same as _variables_ in languages such as C. Registers are data in the CPU,
while variables are data in the stack.

Another important register is the `pc` register, which stands for the _program counter_. This register contains the location of the next instruction
to be executed. Any time an instruction is executed, `pc` is incremented by one to the next instruction to be performed.

The `x87` FPU (processor that works in conjunction with the `x86_64` CPU to process _floating point_ values) has the following registers:

* `st0` through `st7`
  * These aren't really registers but more so pointers to the internal FPU _stack_ that is used to process floating point values.
  * They are all 80-bits wide.

With the _SSE_ extension of the x86 processors, the following registers were added:

* `xmm0` through `xmm15`
  * These are actual registers to store 128-bit wide values (not just floating point values).

Note that these `xmm` registers are not just for floating point values, but any value you'd like to store in a 128-bit wide register. They're
being mentioned specifically in relation to the FPU as they're used in C to pass around floating point values.

To load values into the `st` registers, you must use the stack on the FPU. You also can't load to an `st` register from an existing `r` register
(such as loading the binary equivlent of a floating point number to `rax` and then `fld rax`). You must set the value in the CPU stack (using `rsp`
or `rbp` if it's set to the stack), then load to the FPU stack from there. This is due to the FPU not really having dedicated register components,
but implement them in a stack like fashion (though more like a _revolver_).

Communication is done through memory (the stack). An example would be:

```
push rbp

mov rbp, rsp

sub rsp, 16

mov [rbp-12], dword 0b01000010010100001110000000000000 ; Place the binary number of 52.21875 into the stack.

fld dword [rbp-12]                                     ; Push 52.21875 onto the stack (into st0).

mov rsp, rbp

pop rbp
```

Notice that the above binary value loaded is the actual binary value (following `IEEE 754` standards). See the _Floating point values_ section below.
Note that when interacting with other calls (especially C calls), you use the `xmm` registers to pass on the value. For instance, if we wanted to
store the value of `st0` (say after we performed some arithmetic) into `xmm0`, we would use either `fst` (which simply stores `st0`) or `fstp` (which
stores _and_ pops `st0`). Again, like other floating point instructions, we can't use a register directly. An example of this would be:

```
fstp dword [rbp-20]

movd xmm0, [rbp-20]
```

Note that we use `movd` here instead of `mov`. This is because `xmm` registers are all 128-bit registers. You need to explicitly tell the machine what
size the value is (for 64-bit double precision values it's `movq`).

## Flags

Processors typically have _flags_ stored in a flag register. This register was originally 16-bits wide; however, with 32-bit we got _eflags_ and with
64-bit got _rflags_. Each flag is represented by a bit (either `1` or `0`) to indicate the status of the specific flag. These registers have their
own instructions to manipulate them. The following are the x86 flags described:

For the __flags__ register:

```
Bit # | Mask   | Abbreviation | Description                                                           | Category | is 1 | is 0
------------------------------------------------------------------------------------------------------------------------------
0     | 0x0001 | CF           | Carry Flag (used by add, sub, shift, etc.)                            | Status   | CY   | NC
1     | 0x0002 | N/A          | Reserved, always 1 in EFLAGS                                          | N/A      | N/A  | N/A
2     | 0x0004 | PF           | Parity Flag (used by arithmetic instructions)                         | Status   | PE   | PO
3     | 0x0008 | N/A          | Reserved                                                              | N/A      | N/A  | N/A
4     | 0x0010 | AF           | Adjust Flag (used by add)                                             | Status   | AC   | NA
5     | 0x0020 | N/A          | Reserved                                                              | N/A      | N/A  | N/A
6     | 0x0040 | ZF           | Zero Flag (mainly used by conditional instructions)                   | Status   | ZR   | NZ
7     | 0x0080 | SF           | Sign Flag (mainly used by arithmetic instructions)                    | Status   | NG   | PL
8     | 0x0100 | TF           | Trap Flag (used by debuggers to step through instructions one by one) | Control  | N/A  | N/A
9     | 0x0200 | IF           | Interrupt Enable Flag (respond to maskable interrupts)                | Control  | EI   | DI
10    | 0x0400 | DF           | Direction Flag (direction of string byte copying)                     | Control  | DN   | UP
11    | 0x0800 | OF           | Overflow Flag (determines if overflow occured during arithmetic)      | Status   | OV   | NV
12-13 | 0x3000 | IOPL         | I/O Privilege Level                                                   | System   | N/A  | N/A
14    | 0x4000 | NT           | Nested Task Flag                                                      | System   | N/A  | N/A
15    | 0x8000 | N/A          | Reserved                                                              | N/A      | N/A  | N/A
```

For the __eflags__ register:

```
Bit # | Mask       | Abbreviation | Description                   | Category | is 1 | is 0
------------------------------------------------------------------------------------------
16    | 0x00010000 | RF           | Resume Flag                   | System   | N/A  | N/A
17    | 0x00020000 | VM           | Virtual 8086 Mode Flag        | System   | N/A  | N/A
18    | 0x00040000 | AC           | Alignment Check               | System   | N/A  | N/A
19    | 0x00080000 | VIF          | Virtual Interrupt Flag        | System   | N/A  | N/A
20    | 0x00100000 | VIP          | Virtual Interrupt Pending     | System   | N/A  | N/A
21    | 0x00200000 | ID           | Able To Use CPUID Instruction | System   | N/A  | N/A
22-31 | 0xFFC00000 | N/A          | Reserved                      | System   | N/A  | N/A
```

For the __rflags__ register:

```
Bit # | Mask               | Abbreviation | Description | Category | is 1 | is 0
--------------------------------------------------------------------------------
32-63 | 0xFFFFFFFF00000000 | N/A          | Reserved    | N/A      | N/A  | N/A
```

The x87 FPU also has its own set of 16-bit flags (since it's essentially a processor itself), they are:

```
Bit # | Abbreviation | Description                      | is 1                       | is 0
------------------------------------------------------------------------------------------------------------------
0     | I            | Invalid Operation Exception Flag | Invalid Operation          | No Invalid Operation
1     | D            | Denormalized Exception Flag      | Denormalized               | Not Denormalized
2     | Z            | Zero Divide Flag                 | Divide By Zero             | Not Divide By Zero
3     | O            | Overflow Exception Flag          | Overflow                   | No Overflow
4     | U            | Underflow Exception Flag         | Underflow                  | No Underflow
5     | P            | Precision Exception Flag         | Precision Lost             | Precision Not Lost
6     | SF           | Stack Fault Flag                 | Stack Fault                | No Stack Fault
7     | IR           | Interrupt Request Flag           | Exception is being handled | Exception handling complete
8-10  | C0-C2        | Condition Code Flags             | Condition dependent        | Condition dependent
11-13 | TOP          | TOP Fields                       | Instruction dependent      | Instruction dependent
14    | C3           | Condition Code Flag              | Condition dependent        | Condition dependent
15    | B            | FPU Is Busy Flag                 | FPU Is Busy                | FPU Is Not Busy
```

Note that the error flags are only set when there is an error. If you're going to check them, you must clear them all (`finit`) or clear the specific flag (`fclex`).

Flag registers are modified by pushing and poping its values onto the stack (`pushf`/`popf` for 16-bit, `pushfd`/`popfd` for 32-bit, and `pushfq`/`popfq` for 64-bit).
The following is an example of toggling the `DF` flag (_direction flag_):

```
pushf          ; Push the flags register onto the stack (first 16-bits of eflags/rflags).

pop ax         ; This is a 16-bit flag, so use register ax to pop the flags data into.

push ax        ; Place the original flags onto the stack for later restoration.

xor ax, 0x0400 ; Toggle DF flag (if it was "downwards", then make it "upwards").

push ax        ; Push the modified flags onto the stack.

popf           ; Set new modified flags for use.

...

popf           ; Restore the original flags value.
```

The x87 FPU also contains flags and it works very similar to the flags above; however, a special note is that the FPU does not contain flags for _jump_ calls.
You must first load the result of a comparison (`fcom` for example) into the x86 flag registers before calling a jump (i.e. `je`, `jl`, `jg`, etc.). An example
of this is:

```
ftst                       ; Test st0 against 0.0.                                 

fstsw ax                   ; Load the FPU's flags into ax (since they're 16-bit and ax is 16-bit).

fwait                      ; Wait for the flags to load since it may take a few cycles.

and ax, 0b0100010100000000 ; Isolate the C3, C2, and C0 flags (they're the three used for FPU comparaisons).

cmp ax, 0b0000000100000000 ; Check the flags to see if they're 0, 0, and 1 respectivly (this means "less than" in the FPU).

je foo_bar                 ; Jump if st0 was less than 0.
```

Notice that we use `fwait` here. Because the FPU is an external chip, if an instruction/class requires multiple cycles to complete, the CPU could end up executing
code before the FPU is finished. An example is the `fstsw` instruction which could take a few cycles to finish setting its flags.

## The stack

In assembly programs there is a special location of memory called the _stack_ where raw data is stored of different sizes. The stack is maintained
through the `rsp` register that always points to the top of the stack. Now, you can technically modify `rsp` and use it just like any other register;
however, this will result in possible issues and bugs, so it is recommended to stay away from using it outside of modifying the stack. The stack works
like so (on x86\_64 systems):

```
           s
+ [=========----------] -
```

The `s` here represents the `rsp` register (current location in the stack), the `=` characters represent occupied memory, and `-` is unoccupied memory.
Think of it as a pointer pointing to the top of the stack. Where you perform a `push` operation, it pushes data (values, either from registers,
constants, memory, etc.) onto the stack. For instance, a `push rax` would increment the stack by 8 bytes (since we're in 64-bit mode with x86\_64)
and store the value inside the stack, so now it would look like so:

```
                   s
+ [=================--] -
```

For a `pop` operation, it performs the opposite of a `push`. It takes the value at the top of the stack and places it into the given register, i.e.
a `pop rax` would store the last 8 bytes (since weire in 64-bit mode with x86\_64) into `rax` and move the stack pointer, so now it would look like so:

```
           s
+ [=========----------] -
```

Now notice the `+` and `-` symbols in these diagrams. This is important on the x86\_64 architecture (in combination with Linux). Intuitivly you would
think that a `push rax` instruction would __increment__ the stack by 8 bytes, and when you do a `pop rax`, it would __decrement__ the stack by 8 bytes.
This is not true in this environment. It is the other way around. So when you perform a `push rax`, you are actually __decrementing__ the stack pointer
`rsp` by 8 bytes, and when you perform a `pop rax` you are __incrementing__ the stack pointer `rsp` by 8 bytes. `push` and `pop` at the end of the day
are basically syntactic sugar, for example:

```
push rax

...

pop rax
```

is equivelent to

```
sub rsp, 16

mov [rsp], rax

...

mov rax, [rsp]

add rsp, 16
```

Another important note is the stack _alignment_. Some processors (__not__ x86\_64, but others) require stacks to be aligned for efficient access of the
memory (in the case of x86\_64, it's a 64-bit/8-byte processor so the stack must be aligned by 8-bytes to be efficient). Like noted, x86\_64 will allow
an unaligned stack; however, the code is much less efficient.

With that said, from now on our alignment will be to __16-bytes__ and not 8-bytes. This is because, while technically 8-bytes is aligned, the ABI specifies
that we must align the stack to 16.

Also note that for function calls, when you `call` a function, the return address is `push`'ed to the stack (in 64-bit addresses are 8 bytes, dependent
upon the mode you're in). This means if you want to use the stack, you should always `push` the base location in the stack `rbp` and then move (`mov`)
`rsp` into `rbp` to move around the stack. During this process of using the stack within a function, the stack pointer (`rsp`) should be moved (in bytes)
enough to make room for the data needed in the function. Keep note that in order to keep things align, you should always decrement the size aligned
by 16 bytes, so instead of decrementing by 12 (`0xc`), you'd want to decrement it by 16 (`0x10`).

`push` will allow you to place 64-bit and 16-bit registers onto the stack (you can never push 32-bit or 8-bit registers onto the stack). For instance,
`push rax` and `push ax` will succeed to put data on the top of the stack, but `push eax` will throw an assembler error. This is considered a __bad__
practice however as you don't unalign the stack.

It is also possible to push other byte sizes into the stack by using immediate values. For instance, `push byte 1` will push a single byte onto the stack
and decrement `rsp` by 1 byte (instead of the recommended 8 bytes).

When reading values from the stack, we use a form of _dereferencing_ similar to C. So, say our base pointer `rbp` is pointing to location 0x00004332 on
the stack. We would load a 4-byte value to it using a `mov` instruction, i.e. `mov [rbp-4], dword 58`. Notice that we don't move it directly to `[rbp]`. This
is because when we read/write to/from the stack, it is always _downwards_ instead of _upwards_. So we tell the system to start loading the value 4 bytes
(since it's a 4 byte value) below the base pointer and write up to the base pointer itself. Reading is the same way. We would do something like
`mov eax, [rbp-4]`. Notice we use `eax` instead of `rax`. This is because `eax` is a 32-bit register and `rax` is 64-bit. `rax` would read 8-bytes from
the stack instead of 4-bytes.

Something else to note is that we usually use `mov` to move values between registers. The same goes for the stack as well, i.e. `mov rax, [rbp-8]` will
move the __value__ of `rbp-8` into `rax`. Now if you'd like to refer to a register or the stack by another register (in other words, mov a pointer for a value
to another register), you use the `lea` instruction, i.e. `lea rax, [rbp-8]` will move the __address__ into `rax`.

One more tricky thing about the stack is the byte size in regards to registers. Consider the following example:

```
push rbp

mov rbp, rsp

sub rsp, 16

mov rax, 123456789

mov [rbp-4], dword 56789

mov [rbp-8], dword 12345

mov rax, [rbp-8]

mov rsp, rbp

pop rbp
```

Here we move the value `56789` (a double word, or 4 byte value) into the stack 4 bytes below the base pointer. Then we do the same with `56789`, but 8 bytes
below the base pointer. Now, innocently enough we attempt to move our 4 byte word `12345` into the `rax` register. The problem is here that we are moving
the value at the stack into a 64-bit register (8 bytes). So instead of `rax` being `12345`, it is now `243906897784889` (`12345` is `0x3039` in hexedicimal,
and `56789` is `0xDDD5` in hexidecimal, and `rax` is now `0xDDD500003039`, which is the combination of the two values in hex). What happened is we loaded
both our double word (32-bit) values into `rax` because it is a 64-bit register. We can fix this by replacing the `mov rax, [rbp-8]` with `mov eax, [rbp-8]`
since `eax` is the lower 32-bits of `rax`. __NOTE__: this only works with 32-bit registers (`e` prefix), moving/loading into 16-bit (`ax`) or 8-bit
(`al` and `ah`) will __not__ clear out the upper bits of memory. Observe the following example of this:

```
push rbp

mov rbp, rsp

sub rsp, 16

mov rax, 123456789

mov [rbp-2], word 1234

mov [rbp-4], word 5678

mov ax, [rbp-4]

mov rsp, rbp

pop rbp
```

We loaded `123456789` into `rax`, then we loaded our two 16-bit values onto the stack. As we can see, when we load `5678` into `ax`, the `rax` register
does not have its higher bits cleared, we end up with the value `0x75B162E` in `rax`, which it was originally `0x75BCD15`, but `0xcd15` was replaced with
`0x162E`.

## Functions

Assembly has the concepts of _functions_, though they're called _calls_. Essentially instead of a calling a piece of code with parameters, you would first
fill out the parameters and then use the `call` instruction to run them, i.e. if a call requires two values as its parameters, it might require the first
in `rdi` and the other in `rsi` (similar to how the ABI specifies function calls on x86\_64):

```
mov rsi, 5

mov rdi, 10

call add_two
```

The return value of the function can be stored in any register; however, per the ABI mentioned above it would be in the `eax` registers. So `eax` from
the example above will contain `15`.

When a call is made, the return pointer to the location after the call was made is pushed onto the stack. On 32-bit systems it takes 4 bytes, on 64-bit systems it
takes 8 bytes. So the stack pointer at the start of the function must be preserved before working if that stack (if you do). Typically this is done
using the `rbp` register instead of the `rsp` register (as modifying the current stack pointer will break the stack). A function would work as followed:

```
_start:
  mov rdi, 5

  call foo

foo:
  push rbp

  ...

  pop rbp

  ret ; On return, the return address is pop'ed off the stack and used to jump back to the line for 'call foo'.
```

The latter is fine if you're not calling any other functions from your routine; however, if you are, then there's a possibility that the called routine
will override your local variables you create in the stack. So in accordance with the typical ABI spec, you manage a local _frame_ for your variables.
This is done by performing the previous operation of `push`ing `rbp` and `mov`ing `rsp` into it, and then subtracting the total bytes taken by your local
variables from the _stack_. This will ensure that the next routine you call will have its own space to place its local variables
onto the stack. Keep in mind that your function should also keep the stack aligned (so even if you're only using 3 local 1 byte values, you should still
subtract 4 bytes instead of 3 from the stack). An example is:

```
foo:
  push rbp

  mov rbp, rsp

  sub rsp, 16           ; again, even though the 3 variables below plus the return address only total 12 bytes, we still align to 16.

  mov [rbp-4], dword 1  ; first integer (4 bytes).

  mov [rbp-8], dword 2  ; second integer (4 bytes).

  mov [rbp-12], dword 3 ; third integer (4 bytes).

  ...

  mov rsp, rbp          ; put the stack pointer back to its original location. you could also do: add rsp, 16

  pop rbp               ; put the return address back into rbp.

  ret                   ; return from the function.

bar:
  push rbp

  mov rbp, rsp

  sub rsp, 16

  mov [rbp-4], dword 4

  mov [rbp-8], dword 5

  call foo

  mov rsp, rbp

  pop rbp

  ret
```

Notice that instead of adding the number of bytes for the frame back to `rsp`, we simply place `rbp` back into `rsp`. That's because this is
the intention of the `rbp`/`rsp` relationship. The `rbp` register is not meant to be modified, just simply point to the current stack frame.
This means that in order to go back to the original location of the stack, you just simply set `rsp` back to `rbp`.

For floating point values, registers `xmm0` through `xmm15` are weaved in to the parameters when following the _ABI_. For instance, if we
have a C function with the signature of `void foo(int a, double b, int c)`, the parameter order would be: `rdi`, `xmm0`, and `rsi`.

## System calls

System calls are special functions in the kernel that can be used to perform kernel specific system operations. In the case of Linux, it follows
this format:

* Call Id's are stored in `rax`. These identify which system call to make.
* Parameters for calls are stored in the respective registers: `rdi`, `rsi`, `rdx`, `rcx`, `r8d`, and `r9d`.
* All return values are provided in the `rax` register after a call.

The call routine is expected to preserve the following registers: `rsp`, `rbp`, `rbx`, `r12`, `r13`, `r14`, and `r15`.

System calls are expected on all systems to use the `syscall` keyword after filling out the required registers; however, this
is just syntactic sugar and you can invoke a call using your standard interrupt. On Linux, it's `0x80`, so `int 0x80` will
trigger a system call.

An example system call that will write the given message `msg` with its message length `msg_len` to `stdout`:

```
mov rax, 1         ; write has a syscall id of 1.

mov rdi, 1         ; 1 is for stdout.

mov rsi, msg       ; msg is a db character string.

mov rdx, msg_len   ; msg_len is the constant length of msg.

syscall            ; calls the function, could also use: int 0x80
```

Referencing the system file `read_write.c` for the Linux kernel (and searching for the word `SYSCALL_DEFINE`), we see the signature of the function is:
`size_t write(unsigned int fd, const char *buf, size_t count)`, where `fd` is the file descriptor (`0` for `stdin`, `1` for `stdout`, and `2` for `stderr`).

## Sections

A __section__ (also known as a _segment_) is a directive that indicates in which part of the output file (binary file) the section code will go into. There appear to
be 3 standard sections (`.data`, `.bss`, `.text`).

The following are the keywords for the different _SIZE_'s (_like_ types) in `.data` and `.bss`:

* `b`
  * Byte (8-bits)
* `w`
  * Word (16-bits)
* `d`
  * Double Word (32-bits)
* `q`
  * Quad Word (64-bits)

### Data

Initialized data goes into the `.data` section. This section contains initialized static variables, i.e. global variables and local static variables. They take the format of:
`[label] d[SIZE] [value]`

Examples of this are:

```
msg db "Hello World", 0xA, 0x0

array_int dw 100, 120, 87, -123
```

There is also a `times` optional to generate an array with default values.

```
array_zero_int times 100 dw 0
```

Another keyword is `equ` which can be seen as constant variables. The syntax is in the form of: `[label] equ [value]`.

Examples of this are:

```
buffer_size equ 1024

array_count equ 3
```

A common symbol used in this section is `$`. This indicates the position where the data currently is. This is useful in situations where you may need to calculate the length
of a string, i.e.

```
section .data
  msg db "Hello World", 0xA, 0x0

  msg_length equ $ - msg
```

Here `msg` is just the position where the `msg` data is, so we can use `$` to subtract the position of `msg` from to get the length of the string.

### Bss

Uninitialized data goes into the `.bss` section. This section contains uninitialized static variables, i.e. global variables and local static variables. They take the format of:
`[label] res[SIZE] [value]`

Examples of this are:

```
buffer resb 64

array_int resw 8
```

### Text

The actual code of the program goes into this section. In other assemblers this is also known as `.code`. It takes the format of:

```
section .text
  [CODE HERE]
```

Code can be seperated into sub sections using labels, i.e.:

```
_start:
  [DO SOME PROCESSING]

  jmp exit

exit:
  [CALL EXIT SYSCALL]
```

Note that on Linux systems, the main entry point of an application is `_start`. This label must also be _exposed_ outside the application.
This is done using the `global` keyword. All labels can be exposed, but `_start` specifically must always be exposed, i.e.:

```
section .text:
  global _start

_start:
  [CODE HERE]
```

## Integration with C

Exporting _functions_ to __C__ is similar to how system calls are made and defined.

* Function names are _labels_.
* Parameters are passed in the following order on Linux systems: `rdi`, `rsi`, `rdx`, `rcx`, `r8`, `r9`, the rest are on the stack (`rsp`) starting at `+8`.
  * Floating point numbers are passed using registers `xmm0` through `xmm15`. The order is mixed in with the `r` registers, i.e. if you have a function with
    the signature `void foo(int a, double b, int c)` the ordering is `rdi`, `xmm0`, then `rsi`.
* Return values are stored in `rax`.

An example of integration on `x86_64` Linux systems is:

```
main.asm
--------

; No data or anything needed, just start the code section.
section .text
  ; Expose the 'write' function to C.
  global write

  ; Expose the 'exit' function to C.
  global exit

write:
  ; Push both r10 and r11 onto the stack since we're using them temporarily.
  push r10

  push r11

  ; Move the first and second parameter of the function into r10 and r11 since we'll need them for the 'write' syscall.
  mov r10, rdi

  mov r11, rsi

  ; Call the write syscall.
  mov rax, 1

  mov rdi, 1

  mov rsi, r10

  mov rdx, r11

  ; Pop both r10 and r11 (in LIFO order) to recover the registers.
  pop r11

  pop r10

  syscall

  ; Return. Value of syscall is already in 'rax'.
  ret

exit:
  ; Call the exit syscall.
  ; Note: no need to push or use temporary registers, can store first parameter (rdi) directly into rbx.
  mov rbx, rdi

  mov rax, 60

  syscall

  ret

main.c
------

// Tell the compiler both write and exit are external functions.
extern void write(char *buffer, unsigned long length);

extern void exit(int return_code);

// While not really the entry point, we will declare it as one via the linker
// options.
int main() {
  write("Hello World!\n", 13);

  exit(0);

  return 0;
}
```

And to compile/link the full application, use the following commands:

```
# To assemble the NASM code.
nasm -f elf64 -o main.asm.o main.asm

# To compile the C code.
gcc -g -O0 -nostdlib -c -o main.o main.c

# To link both object files together.
ld -e main -o main.x main.asm.o main.o
```

And when we run our application we get the following output:

```
cwink@CWINK-LINUX:/usr/local/src/nasm$ ./main.x
Hello World!
```

## Endian

Byte ordering is important for data storage. There are two different type's:

* Big-endian
  * Byte order where the most significant byte is first (the "normal" way of ordering).
* Little-endian
  * Byte order where the least significant byte is first.

Let's take the example of a 32-bit _unsigned integer_: `279231491`.

In a Big-endian system, this is stored as `0x10A4BC03`. This would be the standard (or "normal") way of ordering things
as it goes from left to right, which can be seen from the value of `0x10A4BC02` being less than `0x10A4BC03` in decimal and
hex form (`279231490`).

In a Little-endian system, this is stored a `0x03BCA410`, in other words, backwards.

## Negative values

Binary numbers are all handled as if they're _unsigned_ values. In order to deal with _signed_ values, we must use some sort
of way to handle storing signed values as unsigned. There are a few ways, but two of the most popular ones are _ones complement_
and _twos complement_. With ones complement, we simply _negate_ the unsigned value to receive our signed value, i.e. `0b00001100`
is `12` unsigned, so to get `-12`, we negate it to `0b11110011`. Now while this is simple, it makes addition a little more work
after it is stored. In other words, we have a _carry over_ bit when performing addition on a ones complement value, so we can't
just simply perform the following action:

```
mov al, 0b01111011 ; 123.

mov bl, 0b11110011 ; -12 (one's complement).

add al, bl         ; resolves to -110, which is incorrect.
```

In order to fix the value, we need to add the carry over bit, like so:

```
add al, 0x1 ; we have to add in the carry over to get -111 (the correct answer).
```

Which is not ideal as it requires an additional instruction to perform arithmetic. Another issue of concern is that with ones
complement, we don't have a real _zero_ value. There are two, `-0` and `0`. This is problematic and a waste of space, meaning
we only have a value range of `-127` to `127` with a signed value. This leads us to the more popular version the _twos complement_.
This works in the same way as the _ones complement_, as in we negate the value; however, we also add `1` to the negated value.
This means two things:

1. We no longer have a `-0` and `0`, _zero_ is always `0`.
2. We no longer have to account for a carry over with addition.

To visualize this, consider the following code:

```
mov al, 0b01111011 ; 123.

mov bl, 0b11110100 ; -12 (two's complement).

add al, bl         ; resolves to -111, which is correct.
```

We no longer need the additional instruction for arithmetic. This is also beneficial for the range of the signed value, as twos complement
has a range of `-128` to `127`.

## Floating point values

On x86/x86\_64 processors, there is a seperate processor that deals specifically with _floating point_ numbers (the __x87 FPU__). This
is mainly because fractional numbers a very complex and having a seperate processor for them drastically speeds up arithmetic on them,
so much that there is even an instruction `fsqrt` to calculate the square root of a number. Storing these values in binary face the same
problem as dealing with negative values. There's no native support in binary arithmetic for floating point values, so the _x87_ uses
the _IEEE 754_ standard to define how floating point values are store. They are slightly different depending upon the bit size of the
number (usually 32-bit or 64-bit). We'll be discussing 32-bit here.

Floating point values are basically store as 3 different parts from _left_ to _right_. The three parts are:

* Sign
  * 1-bit value determining whether the number is negative or positive.
* Exponent
  * 8-bit value that is used to move the decimal place in the _mantissa_. It has a bias (`127` for 32-bit, `1203` for 64-bit). This bias
    is _subtracted_ from the exponent during calculation. It is __not__ stored in memory.
* Mantissa
  * 23-bit value representing the fractional component (with the first `1` bit omitted). The first bit (_significand_) is omitted because
    it will always be a value with the following restraints: `1.0 <= significand < 2.0`.
  * This value is calculated by spliting the _integral_ part of the number and the _fractional_ part of the number. To get the binary representation
    of the fractional part, you multiply it by 2 continously (using only the fractional part of every result) until you reach `1.00`. See example
    below.

The design is very similar to how _scientific notation_ is performed. The formula that may be derived would be
`x = (-1)^Sign * (1.Mantissa)_2 * 2^(Exponent - Bias)`.

For example, let's convert `52.21875` to it's binary equivelent:

* Sign
  * This will be `0` as it's a positive value.
  * Binary: `0`
* Exponent
  * There is a length of `5` for the precision, so adding `5` to the bias `127`, we get `132`.
  * Binary: `10000100`
* Mantissa
  * Integral is `52`
    * Binary: `110100`
  * Fractional is `0.21875`
    * Calculation goes as follows: `0.21875 * 2 = 0.4375`, `0.4375 * 2 = 0.875`, `0.875 * 2 = 1.75`, `0.75 * 2 = 1.5`, `0.5 * 2 = 1`.
    * Binary: `00111`
  * Combined integral and fraction is `110100.00111`
    * Removing significand we get `10100.00111`
    * Binary: `1010000111`

Putting this all together we get a total binary value of `01000010010100001110000000000000`. Reversing the process would be:

* Sign
  * Binary: `0`
  * Decimal: `0`
* Exponent
  * Binary: `10000100`
  * Decimal: `132`
  * Minus Bias: `5`
* Mantissa
  * Binary: `1010000111`...
  * Added significant: `1.1010000111`
  * Move by exponent (`5`): `110100.00111`

Now for the Mantissa, we split if over the decimal and perform normal binary conversion on the _left_, and inverse binary conversion on the _right_.
To show this, we do: `(1 * 2^5) + (1 * 2^4) + (0 * 2^3) + (1 * 2^2) + (0 * 2^1) + (0 * 2^0)` . `(0 * 2^-1) + (0 * 2^-2) + (1 * 2^-3) + (1 * 2^-4) + (1 * 2^-5)`,
thus we get `52.21875`.
