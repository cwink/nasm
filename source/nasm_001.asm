;
; This is a simple "Hello World" program. It basically creates a static Hello World string and then calls write and exit.
;

section .data
  msg db "Hello, World!", 0xA, 0x0

  msg_len equ $ - msg

section .text
  global _start

_start:
  ; Here we want to call the 'write' function from the Linux kernel. It's signature is:
  ;
  ;   write(unsigned int fd, const char *buf, size_t count) [read_write.c]
  ;
  ;     fd = 0 (STDIN), 1 (STDOUT), 2 (STDERR)
  ;
  ;     buf = message string
  ;
  ;     count = length of buffer (buf)
  mov rax, 1

  mov rdi, 1

  mov rsi, msg

  mov rdx, msg_len

  syscall

  ; Here we want to call the 'exit' function from the Linux kernel. It's signature is:
  ;
  ;   exit(int error_code) [exit.c]
  ;
  ;     error_code - exit code.
  mov rax, 60

  mov rdi, 0

  syscall
