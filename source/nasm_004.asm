;
; This program will take a string and reverse its contents.
;

section .data
  SYSCALL_WRITE equ 1
  SYSCALL_EXIT equ 60

  STDOUT equ 1

  msg db "Hello World!"

  msg_len equ $ - msg

section .bss
  buf resb msg_len + 1

section .text
  global _start

_start:
  mov rsi, msg 

  xor rcx, rcx

  ; reset the "df" flag to zero, making all string instructions work on strings from left to right.
  cld

  call str_length

  xor rax, rax

  xor rdi, rdi

  call reverse_str

  call print_result

  jmp exit

str_length:
  pop rdi

str_length_next:
  cmp [rsi], byte 0

  je str_length_exit

  ; this is a string instruction that loads a byte from rsi into al and then increments rsi by one.
  lodsb

  push rax

  inc rcx

  jmp str_length_next

str_length_exit:
  push rdi

  ret

reverse_str:
  pop r10

reverse_str_next:
  cmp rcx, 0

  je reverse_str_exit

  pop rax

  mov [buf + rdi], rax

  dec rcx

  inc rdi

  jmp reverse_str_next

reverse_str_exit:
  push r10

  ret

print_result:
  mov rax, SYSCALL_WRITE

  mov rdx, rdi

  mov rdi, STDOUT

  mov rsi, buf

  syscall

  mov rax, SYSCALL_WRITE

  mov rdx, 1

  mov rdi, STDOUT

  mov rsi, 0xa

  syscall
  
exit:
  mov rax, SYSCALL_EXIT

  mov rdi, 0

  syscall
