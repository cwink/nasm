;
; This is a simple 'compare' example program that will compare the sum result of num_1 and num_2 with 150. If so,
; it will output "Sum is correct", else it just exists.
;
section .data
  num_1: equ 100

  num_2: equ 50

  msg db "Sum is correct", 0xA, 0x0

  msg_len equ $ - msg

section .text
  global _start

_start:
  ; Load num_1 and num_2 into rax and rbx respectivly for addition processing (basically: rax = num_1 and rbx = num_2).
  mov rax, num_1

  mov rbx, num_2

  ; Add rax and rbx, then store the result into rax (similar to rax += rbx).
  add rax, rbx

  ; Compare rax (100 + 50 = 150) to the number 150. The keyword 'cmp' in the processor does the comparison and stores a temporary
  ; 'true' or 'false' for the next instruction (line) to use (i.e. 'je' for if cmp is true, 'jne' if cmp is not true, etc.).
  cmp rax, 150

  ; If the previous condition is not equal (false), then goto the "exit" label.
  jne exit

  ; By default if the previous condition is not met (notice, this isn't an "else" or "else if"), then move on.
  jmp sum

sum:
  ; Call the write syscall.
  mov rax, 1

  mov rdi, 1

  mov rsi, msg

  mov rdx, msg_len

  syscall

  jmp exit
  
exit:
  ; Exit the system via the exit syscall.
  mov rax, 60

  mov rdi, 0

  syscall
  
