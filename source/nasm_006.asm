section .text
  global _start

_start:
  push rbp

  mov rbp, rsp

  sub rsp, 24

  mov [rbp-12], dword 0b01000010010100001110000000000000 ; Place the binary number of 52.21875 into the stack.

  mov [rbp-16], dword 0b00111111101000000000000000000000 ; Place the binary number of 1.25 into the stack.

  fld dword [rbp-12] ; Push 52.21875 onto the stack (into st0).

  fld dword [rbp-16] ; Push 1.25 onto the stack (move st0 into st1, then 1.25 into st0).

  fadd ; Add st1 to st0 and pop st1 off the stack (to st7), resulting in the value of 53.46875.

  fstp dword [rbp-20] ; Take the result from st0, store it at [rbp-20] and pop the value to the end of the stack (st7).

  movd xmm0, [rbp-20] ; Move the result into register xmm0, note that we use 'movd' instead of 'mov' since we're moving a double work (32-bit, float).

  mov rsp, rbp

  pop rbp

  mov rax, 60

  mov rdi, 0

  syscall
