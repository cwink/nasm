;
; This code mainly deals with learning macro's in NASM.
;

; define standard descriptors.
%define STDIN 0

%define STDOUT 1

%define STDERR 2

; declare a SYSCALL_WRITE macro that takes 2 parameters and calls the 'write' system call.
%macro SYSCALL_WRITE 2
  ; jump to our call label (notice we declare and reference labels with %%) because we define data afterwards.
  jmp %%call

  ; define message data.
  %%msg db %2, 0

  %%msg_len equ $ - %%msg

%%call:
  ; push all modifiable registers to stack.
  push rax

  push rdi

  push rsi

  push rdx

  push r11

  ; call syscall 'write' using STDOUT
  mov rax, 1

  mov rdi, %1

  mov rsi, %%msg

  mov rdx, %%msg_len

  syscall

  ; pop all registers off stack.
  pop r11

  pop rdx

  pop rsi

  pop rdi

  pop rax
%endmacro

; declare a SYSCALL_EXIT macro that takes 1 argument and calls the 'exit' system call.
%macro SYSCALL_EXIT 1
  mov rax, 60

  mov rdi, %1

  syscall
%endmacro

; syntactic sugar for SYSCALL_WRITE.
%macro PRINT 2
  SYSCALL_WRITE %1, %2
%endmacro

; prints text with a new line at the end using SYSCALL_WRITE.
%macro PRINT_LINE 2
  SYSCALL_WRITE %1, %2

  SYSCALL_WRITE %1, 0xa
%endmacro

section .text
  global _start

_start:
  PRINT_LINE STDOUT, "Hello World!"

  PRINT_LINE STDOUT, "Again!"

  SYSCALL_EXIT 0
