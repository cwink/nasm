;
; This is a program that takes two string arguments from the command prompt and adds them together and prints them.
;

section .data
  SYSCALL_WRITE equ 1

  SYSCALL_EXIT equ 60

  STDOUT equ 1

  STDERR equ 2

  EXIT_CODE equ 0

  FAILED_EXIT_CODE equ 1

  wrong_argc_msg db "Must be two command line arguments.", 0xA, 0x0

  wrong_argc_msg_len equ $ - wrong_argc_msg

section .text
  global _start

_start:
  ; pop the top of the stack (rsp) into rcx (which is argc).
  pop rcx

  ; check to ensure there are 2 arguments, if not, error out.
  cmp rcx, 3

  jne argc_error

  ; increment the stack by 8 bytes to argv[1] (since it was at argv[0] after the pop).
  add rsp, 8

  ; pop the top of the stack into rsi (which is argv[1] at this point).
  pop rsi

  ; call our str_to_int function with parameter rsi.
  call str_to_int

  ; move our return value from rax into r10.
  mov r10, rax

  ; pop the top of the stack again (which is argv[2] at this point).
  pop rsi

  ; call our str_to_int function again with new parameter rsi.
  call str_to_int

  ; move the return value rax into r11.
  mov r11, rax

  ; add the two return values together.
  add r10, r11

  ; move result to rax for int_to_str.
  mov rax, r10

  call int_to_str

  ; print the integer to the screen.
  call print

  mov rdi, 0

  jmp exit

str_to_int:
  ; set rax to 0 since it's our return value.
  xor rax, rax

  ; move 10 into rcx for start multiplier with position in integer.
  mov rcx, 10

str_to_int_next:
  ; compare the first byte in rsi to 0 (which is '\0', null terminated value), and return if so.
  cmp [rsi], byte 0

  je str_to_int_return

  ; move the current byte from rsi (argv[1][x]) into the low byte b register.
  mov bl, [rsi]

  ; subtract 48 from bl since '48' is '0' in ASCII, which will give us the actual 0 integer value.
  sub bl, 48

  ; multiply rax by rcx and store the result into rax, notice that rax is implicit in multiply functions and is always used as the first variable/register.
  mul rcx

  ; add rbx (which is the integer obtained from bl) onto rax.
  add rax, rbx

  ; increment the stack by one byte to get the next character in argv[1] (x).
  inc rsi

  ; loop back around.
  jmp str_to_int_next

str_to_int_return:
  ; return from the function with the result stored in rax.
  ret

int_to_str:
  ; we modify the stack so we need to store off the return pointer.
  pop r13

  ; r12 is used to store the length of the string, set it to 1 since the next code pushes a newline character onto the stack.
  mov r12, 1

  ; push a newline character onto the stack.
  mov rdx, 0xa

  push rdx

int_to_str_next:
  ; clear out rdx (must be done for div to work, or else it errors out).
  mov rdx, 0

  ; move our base multiplier 10 to rbx.
  mov rbx, 10

  ; divide rax (obtained from previous call) by rbx, the result gets stored into rax, and the remainder into rdx.
  div rbx

  ; add 48 to the remainder as the remainder will be the current integer character.
  add rdx, 48

  ; push rdx onto the stack.
  push rdx

  ; increment r12 by 1 bytes to keep track of position.
  inc r12

  ; if rax (the value from the division above) is 0, then we're done with out conversion.
  cmp rax, 0

  jne int_to_str_next

  ; put the return pointer back on the stack and return.
  push r13

  ret

print:
  ; SYSCALL_WRITE modifies the stack, so we save off the return pointer.
  pop r13

  ; we use r12 (the length of the string on the stack) to calculate the byte size of the string and store it into rax.
  mov rax, 1
  
  mul r12

  mov r12, 8

  mul r12

  mov rdx, rax

  mov rax, SYSCALL_WRITE

  mov rdi, STDOUT

  ; rsp at this point will contain all the characters pushed in int_to_str.
  mov rsi, rsp

  syscall

  push r13

  ret

argc_error:
  mov rax, SYSCALL_WRITE

  mov rdi, STDERR

  mov rsi, wrong_argc_msg

  mov rdx, wrong_argc_msg_len

  syscall

  mov rdi, 1

  jmp exit

exit:
  mov rax, SYSCALL_EXIT

  syscall

