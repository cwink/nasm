#include "cwlib.h"

void _start() {
  char buffer[15] = {'H', 'e', 'l', 'l', 'o',  ' ',  'W', 'o',
                     'r', 'l', 'd', '!', '\0', '\0', '\0'};

  char buffer_2[6] = {'\0', '\0', '\0', '\0', '\0', '\0'};

  CWLib_prepend(buffer, 'E');

  CWLib_print_line(1, buffer);

  CWLib_reverse(buffer);

  CWLib_print_line(1, buffer);

  CWLib_append(buffer, 'X');

  CWLib_print_line(1, buffer);

  CWLib_int_to_string(-8337, buffer_2);

  CWLib_print_line(1, buffer_2);

  float result = 0.0F;

  CWLib_power(2.0F, -3.0F, &result);

  CWLib_float_to_string(-12.34, buffer_2);

  CWLib_print_line(1, buffer_2);

  CWLib_exit(0);
}
