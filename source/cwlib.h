#ifndef CWLIB_H
#define CWLIB_H

extern unsigned char CWLib_float_to_string(const float number,
                                           char *const restrict buffer);

extern unsigned char CWLib_power(const float x, const float y,
                                 float *const restrict result);

extern unsigned char CWLib_int_to_string(const int number,
                                         char *const restrict buffer);

extern unsigned char CWLib_length(const char *const restrict string,
                                  unsigned int *const restrict len);

extern unsigned char CWLib_reverse(char *const restrict buffer);

extern unsigned char CWLib_append(char *const restrict buffer,
                                  const char character);

extern unsigned char CWLib_prepend(char *const restrict buffer,
                                   const char character);

extern unsigned char CWLib_print(const unsigned int file_descriptor,
                                 const char *const restrict string);

extern unsigned char CWLib_print_line(const unsigned int file_descriptor,
                                      const char *const restrict string);

extern void CWLib_exit(int code);

#endif /* CWLIB_H */
