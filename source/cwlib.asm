;
; A "library" with a few useful calls that was written to learn x86 assembly more deeply.
;

section .text
  global CWLib_float_to_string

  global CWLib_power

  global CWLib_int_to_string

  global CWLib_length

  global CWLib_reverse

  global CWLib_append

  global CWLib_prepend

  global CWLib_print

  global CWLib_print_line

  global CWLib_exit

;
; Convert an 32-bit floating point value to a null terminated string (buffer).
;
;   params:
;
;     xmm0 - a 32-bit floating point to convert.
;
;     rsi - a pointer to an address that can hold the converted string.
;
;   return:
;
;     rax - 1 if the call was successful, 0 if rsi is null.
;
CWLib_float_to_string:
  cmp rdi, 0

  je CWLib_float_to_string_return_failure

  push rbp

  mov rbp, rsp

  sub rsp, 16

  movd [rbp-4], xmm0

  fld dword [rbp-4]

  push r10

  xor r10, r10

  ftst

  fstsw ax

  fwait

  and ax, 0b0100010100000000

  cmp ax, 0b0000000100000000

  je CWLib_float_to_string_handle_negative

CWLib_float_to_string_handle_negative_return:
  mov rax, rdi

  fst st1

  frndint

  fsub st1, st0

  fxch st0, st1

  ftst

  fstsw ax

  fwait

  and ax, 0b0100010100000000

  cmp ax, 0b0000000100000000

  je CWLib_float_to_string_handle_rounding

CWLib_float_to_string_handle_rounding_return:
  mov [rbp-8], dword 100000
  
  fild dword [rbp-8]

  fmulp st1, st0

  frndint

  fistp dword [rbp-8]

  fistp dword [rbp-4]

  push rbx

  mov ebx, [rbp-8]

  mov eax, [rbp-4]

  push rsi

  mov rsi, rdi

  mov rdi, rax

  call CWLib_int_to_string

  mov rdi, rsi

  lea rsi, [rbp-4]

  call CWLib_length

  push rcx

  mov ecx, [rsi]

  push rdi

  add rdi, rcx

  mov [rdi], byte 46

  inc rdi

  mov rsi, rdi

  mov rdi, rbx

  call CWLib_int_to_string

  pop rdi

  cmp r10, 1

  je CWLib_float_to_string_add_negative

CWLib_float_to_string_add_negative_return:
  pop rcx

  pop rsi

  pop rbx

  pop r10

  mov rsp, rbp

  pop rbp

  mov rax, 1

  ret

CWLib_float_to_string_handle_rounding:
  fld1

  fadd st1, st0

  fsubp st2, st0

  jmp CWLib_float_to_string_handle_rounding_return

CWLib_float_to_string_handle_negative:
  mov r10, 1

  fabs

  jmp CWLib_float_to_string_handle_negative_return
  
CWLib_float_to_string_add_negative:
  mov rsi, 45

  call CWLib_prepend
  
  jmp CWLib_float_to_string_add_negative_return

CWLib_float_to_string_return_failure:
  mov rax, 0

  ret

;
; Calculate the power of a value (x^y).
;
;   params:
;
;     xmm0 - The 32-bit floating point value to get the power of (x).
;
;     xmm1 - The 32-bit floating point value to take the power of (y).
;
;     rdi - The 32-bit floating point value to store the result of x^y in.
;
;   return:
;
;     rax - 1 if the function was successful, 0 if rdi was NULL.
;
CWLib_power:
  cmp rdi, 0

  je CWLib_power_return_failure

  push rbp

  mov rbp, rsp

  sub rsp, 16

  movd [rbp-4], xmm0

  movd [rbp-8], xmm1

  fld dword [rbp-8]

  fld dword [rbp-4]

  fyl2x

  fld st0

  frndint

  fsub st1, st0

  fxch st1

  f2xm1

  fld1

  fadd

  fscale

  fstp st1

  fstp dword [rdi]

  mov rax, 1

  mov rsp, rbp

  pop rbp

  ret

CWLib_power_return_failure:
  mov rax, 0

  ret

;
; Convert an integer to a null terminated string (buffer).
;
;   params:
;
;     rdi - a 32-bit integer to convert.
;
;     rsi - a pointer to an address that can hold the converted string.
;
;   return:
;
;     rax - 1 if the call was successful, 0 if rsi is null.
;
CWLib_int_to_string:
  cmp rsi, 0

  je CWLib_int_to_string_return_failure

  push rbx

  push rcx

  push rdx

  mov rbx, rdi

  mov rax, rdi
  
  cmp eax, 0

  jl CWLib_int_to_string_abs

CWLib_int_to_string_abs_return:
  mov rcx, 10

  mov rdi, rsi

  cmp rax, 0

  je CWLib_int_to_string_handle_zero

CWLib_int_to_string_next:
  cmp rax, 0

  je CWLib_int_to_string_post_next

  xor rdx, rdx

  div rcx

  add rdx, 48

  mov [rdi], dl

  inc rdi

  jmp CWLib_int_to_string_next
  
CWLib_int_to_string_post_next:
  mov [rdi], byte 0x0

  mov rdi, rsi

  call CWLib_reverse

  cmp ebx, 0

  jl CWLib_int_to_string_add_negative

  jmp CWLib_int_to_string_return_success

CWLib_int_to_string_handle_zero:
  mov [rdi], byte 48

  inc rdi

  mov [rdi], byte 0x0

CWLib_int_to_string_return_success:
  mov rax, 1

  pop rdx

  pop rcx

  pop rbx

  ret

CWLib_int_to_string_return_failure:
  mov rax, 0

  ret

CWLib_int_to_string_add_negative:
  mov rdi, rsi

  mov rsi, 45

  call CWLib_prepend

  jmp CWLib_int_to_string_return_success

CWLib_int_to_string_abs:
  neg eax

  jmp CWLib_int_to_string_abs_return
  
;
; Retrieve the length of a null terminated string.
;
;   params:
;
;     rdi - a pointer to an address that contains the null terminated string.
;
;     rsi - a pointer to an address of a 32-bit unsigned integer to store the CWLib_length of the string.
;
;   return:
;
;     rax - 1 if the call was successful, 0 if rdi or rsi are null.
;
CWLib_length:
  cmp rdi, 0

  je CWLib_length_return_failure

  cmp rsi, 0
  
  je CWLib_length_return_failure

  mov rax, rdi

  mov [rsi], dword 0

CWLib_length_next:
  cmp [rax], byte 0

  je CWLib_length_return_success

  inc rax

  add [rsi], dword 1

  jmp CWLib_length_next

CWLib_length_return_success:
  mov rax, 1

  ret

CWLib_length_return_failure:
  mov rax, 0

  ret

;
; Reverses the characters of a null terminated string.
;
;   params:
;
;     rdi - a pointer to an address that contains the null terminated string.
;
;   return:
;
;     rax - 1 if the call was successful, 0 if rdi is null.
;
CWLib_reverse:
  cmp rdi, 0

  je CWLib_reverse_return_failure

  push rbp

  mov rbp, rsp

  sub rsp, 16

  push rcx

  push rdx

  push rsi

  lea rsi, [rbp-4]

  call CWLib_length

  pop rsi

  mov rax, rdi

  mov ecx, [rbp-4]

  add rax, rcx

  dec rax

  mov rcx, rdi

CWLib_reverse_next:
  cmp rcx, rax

  jge CWLib_reverse_return_success

  mov dl, [rax]

  mov dh, [rcx]

  mov [rax], dh

  mov [rcx], dl

  inc rcx

  dec rax

  jmp CWLib_reverse_next

CWLib_reverse_return_success:
  mov rax, 1

  pop rdx

  pop rcx

  mov rsp, rbp

  pop rbp

  ret

CWLib_reverse_return_failure:
  mov rax, 0

  ret

;
; Appends a character to a null terminated string (buffer).
;
;   parameters:
;
;     rdi - a pointer to an address that contains the null terminated string (buffer).
;
;     rsi - the character to append to the string.
;
;   return:
;
;     rax - 1 if the call was successful, 0 if rdi is null.
;
CWLib_append:
  cmp rdi, 0

  je CWLib_append_return_failure

  push rbp

  mov rbp, rsp
  
  sub rsp, 16

  push rdx

  push rcx

  mov rdx, rsi

  lea rsi, [rbp-4]

  call CWLib_length

  mov rax, rdi

  mov ecx, [rbp-4]

  add rax, rcx

  mov [rax], dl

  inc rax

  mov [rax], byte 0x0

  mov rax, 1

  pop rcx

  pop rdx

  mov rsp, rbp

  pop rbp

  ret

CWLib_append_return_failure:
  mov rax, 0

  ret

;
; Prepends a character to a null terminated string (buffer).
;
;   parameters:
;
;     rdi - a pointer to an address that contains the null terminated string (buffer).
;
;     rsi - the character to prepend to the string.
;
;   return:
;
;     rax - 1 if the call was successful, 0 if rdi is null.
;
CWLib_prepend:
  cmp rdi, 0

  je CWLib_prepend_return_failure

  push rbp

  mov rbp, rsp
  
  sub rsp, 16

  push rdx

  push rcx

  mov rdx, rsi

  lea rsi, [rbp-4]

  call CWLib_length

  mov rax, rdi

  mov ecx, [rbp-4]

  add rax, rcx

  inc rax

CWLib_prepend_next:
  cmp rax, rdi

  je CWLib_prepend_return_success

  mov cl, [rax-1]

  mov [rax], cl

  dec rax

  jmp CWLib_prepend_next

CWLib_prepend_return_success:
  mov [rax], dl
  
  mov rax, 1

  pop rcx

  pop rdx

  mov rsp, rbp

  pop rbp

  ret

CWLib_prepend_return_failure:
  mov rax, 0

  ret

;
; Prints the given null terminated string to the given filedescriptor with a newline character.
;
;   parameters:
;
;     rdi - file descriptor, 1 = stdin, 2 = stdout, 3 = stderr.
;
;     rsi - the null terminated string to write.
;
;   return:
;
;     rax - 1 if the call was successful, 0 if rsi is null.
;
CWLib_print_line:
  call CWLib_print

  cmp rax, 0

  je CWLib_print_line_return

  push rbp

  mov rbp, rsp

  sub rsp, 16

  mov [rbp-1], byte 10

  lea rsi, [rbp-1]

  push rdx

  mov rdx, 1

  mov rax, 1

  syscall

  pop rdx

  mov rsp, rbp
  
  pop rbp

  mov rax, 1
  
CWLib_print_line_return:
  ret

;
; Prints the given null terminated string to the given filedescriptor.
;
;   parameters:
;
;     rdi - file descriptor, 1 = stdin, 2 = stdout, 3 = stderr.
;
;     rsi - the null terminated string to write.
;
;   return:
;
;     rax - 1 if the call was successful, 0 if rsi is null.
;
CWLib_print:
  cmp rsi, 0

  je CWLib_print_return_failure

  push rbp

  mov rbp, rsp
  
  sub rsp, 8

  push rdx

  mov rdx, rdi

  mov rdi, rsi

  lea rsi, [rbp-4]

  call CWLib_length

  mov rsi, rdi

  mov rdi, rdx

  mov edx, [rbp-4]

  mov rax, 1

  syscall

  pop rdx

  mov rax, 1

  mov rsp, rbp

  pop rbp

  ret

CWLib_print_return_failure:
  mov rax, 0

  ret

;
; Exits an application.
;
;   params:
;
;     rdi - the 32-bit error code.
CWLib_exit:
  mov rax, 60

  syscall
